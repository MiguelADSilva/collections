package com.miguel.Lists;

import org.junit.Before;
import org.junit.Test;

public class LinkedListTest {
    LinkedList<Integer> list;

    @Before
    public void setUp(){
       list = new LinkedList<Integer>();
    }

    @Test
    public void testMyList() {
    }

    @Test
    public void addFirst() {
        //------------------------
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        //------------------------
        list.display();
        //------------------------
    }

    @Test
    public void removeFirst(){
        //------------------------
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        //------------------------
        list.removeFirst();
        //------------------------
        list.display();
    }

    @Test
    public void addLast(){
        //------------------------
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        //------------------------
        list.addLast(7);
        //------------------------
        list.display();
    }

    @Test
    public void removeLast(){
        //------------------------
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        //------------------------
        list.addLast(7);
        //------------------------
        list.removeLast();
        //-------------------------
        list.display();
    }

    @Test
    public void constains(){
        //------------------------
        list.addFirst(1);
        list.addFirst(2);
        list.addFirst(3);
        //------------------------
        list.addLast(7);
        //------------------------
        System.out.println(list.contains(1));
    }

    @Test
    public void push(){
        list.push(1);
        list.push(2);
        list.push(3);
        //------------------------
        list.display();
    }

    @Test
    public void remove(){
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        //----------------------
        list.display();
        //----------------------
        System.out.println();
        //----------------------
        list.remove(1);
        //----------------------
        list.display();

    }

    @Test
    public void getFirst(){
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        //----------------------
        list.display();
        //----------------------
        System.out.println();
        //----------------------
        System.out.println(list.getFirst());
        //----------------------
    }

    @Test
    public void getLast(){
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        //----------------------
        list.display();
        //----------------------
        System.out.println();
        //----------------------
        System.out.println(list.getLast());
        //----------------------
    }

    @Test
    public void get(){
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        //----------------------
        list.display();
        //----------------------
        System.out.println();
        //----------------------
        System.out.println(list.get(3));
        //----------------------
    }
}
