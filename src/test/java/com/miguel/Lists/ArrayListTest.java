package com.miguel.Lists;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArrayListTest {
    ArrayList<Integer> list;

    @Before
    public void setUp(){
        list = new ArrayList<Integer>();
    }

    @Test
    public void testMyList() {
    }
    @Test
    public void add(){
        System.out.println("Adicionar: ");
        //------------
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //------------
        list.printArrayList();
    }
    @Test
    public void remove(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //-----------------------
        System.out.println("Remove:");
        list.remove(0);
        //------------------------
        list.printArrayList();
        //------------------------
    }
    @Test
    public void clear(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //----------------
        System.out.println(list.isEmpty());
        //----------------
        System.out.println();
        //-----------------
        System.out.println("Clear:");
        list.clear();
        list.printArrayList();
        //----------------
        System.out.println();

    }

    @Test
    public void isEmpty(){
        System.out.println(list.isEmpty());
        //---------------------------------
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //----------------------------------
        list.printArrayList();
        //----------------------------------
        assertFalse(list.isEmpty());
    }
    @Test
    public void get(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //--------------------------
        list.printArrayList();
        //---------------------------
        System.out.println();
        //---------------------------
        System.out.println("Gets: ");
        System.out.println("i(0)="+list.get(0));
        System.out.println("i(1)="+list.get(1));
        System.out.println("i(2)="+list.get(2));
        System.out.println("i(3)="+list.get(3));
        System.out.println("i(4)="+list.get(4));
        //---------------------------
    }

    @Test
    public void addIndex(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //----------------------
        list.printArrayList();
        //----------------------
        System.out.println("Adicionar:");
        list.addIndex(0,50);
        //----------------------
        list.printArrayList();
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void indexOutBounds(){
        list.add(1);
        list.add(2);
        //--------------
        list.get(4);
    }

    @Test
    public void contains(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(4);
        //----------------------
        list.printArrayList();
        //----------------------
        System.out.println(list.contains(4));
    }
}