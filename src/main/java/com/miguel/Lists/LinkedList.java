package com.miguel.Lists;
import java.util.*;

import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {
    Node<T> head;
    Node<T> tail;
    private int size = 0;

    /*addFirst é usado para inserir um elemento especifico no ínicio de uma LinkedList*/
    public void addFirst(T element) {
        Node<T> node = new Node<T>();
        node.value = element;

        if(this.isEmpty()){
            head = node;
            tail = node;
        }else{
            node.next = head;
            head = node;
        }
    }

    /*O display serve para imprimir os elementos da linkedlist */
    public void display(){
        if(head == null) {
            return;
        }
            Node<T> current = head;
            while(current != null){
                System.out.print(current.value + " -> ");
                current = current.next;
            }
        System.out.print(current);
    }

    /*insere um elemento especifico no final da lista*/
    public void addLast(T element) {
        Node<T> node = new Node<>();
        node.value = element;

        if(this.isEmpty()){
            head = node;
            tail = node;
        }else{
            tail.next = node;
            tail = node;
        }
    }


    public boolean offerFirst(T t) {
        return false;
    }


    public boolean offerLast(T t) {
        return false;
    }

    /*Remove e retorna o primeiro elemento da lista*/
    public T removeFirst() {
        if(this.isEmpty()){
            return null;
        }else if(head == tail){
            T node =  head.value;
            head = null;
            tail = null;
            return  node;
        }else{
            T node =  head.value;
            head = head.next;
            return  node;
        }
    }

    /*Remove e retorna o ultimo elemento da lista*/
    public T removeLast() {
        if(this.isEmpty()){
            return null;
        }else if(head == tail){
            T node = head.value;
            head = null;
            tail = null;
            return node;
        }else{
            Node<T> n = head;
            while(n.next.next != null){
                n = n.next;
            }

            T element = n.next.value;
            n.next = null;

            tail = n;
            return element;
        }
    }


    public T pollFirst() {
        return (T) tail;
    }


    public T pollLast() {
        return (T) head;
    }

    /*Retorna o primeiro elemento da lista*/
    public T getFirst() {
        if(head == null){
            throw new NoSuchElementException();
        }
        return head.value;
    }

    /*Retorna o ultimo elemento da lista*/
    public T getLast() {
        if(head == null){
            throw new NoSuchElementException();
        }
        Node<T> tmp = head;
        while(tmp.next != null){
            tmp = tmp.next;
        }
        return tmp.value;
    }


    public T peekFirst() {
        return null;
    }

    public T peekLast() {
        return null;
    }


    public boolean removeFirstOccurrence(Object o) {
        return false;
    }


    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    public boolean offer(T t) {
        return false;
    }


    public T remove() {
        return null;
    }


    public T poll() {
        return null;
    }

    public T element() {
        return null;
    }


    public T peek() {
        return null;
    }

    /*Insere o elemento na frente dessa lista*/
    public void push(T element) {
        Node<T> newNode = new Node<>(element);
        newNode.next = head;
        head = newNode;
    }


    public T pop() {
        return null;
    }


    public Iterator<T> descendingIterator() {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (head == null);
    }

    /*Retorna true se esta lista contiver o elemento especifico or false se nao conter */
    @Override
    public boolean contains(Object o) {
        for(Node<T> n = head; n!=null; n=n.next){
            if(n.value.equals(o)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    /*Retorna um elemento na posição especifica da LinkedList*/
    @Override
    public T get(int position) {
        if(head == null){
            throw new IndexOutOfBoundsException();
        }
        Node<T> tmp = head;
        for(int i = 0; i <position; i++){
            tmp = tmp.next;
        }
        if(tmp == null){
            throw new IndexOutOfBoundsException();
        }
        return tmp.value;
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {

    }

    /*Remove o elemento na posição*/
    @Override
    public T remove(int position) {
        if (head == null) {
            return null;
        } else if (position == 0) {
            return (T) head.next;
        } else {
            Node n = head;
            for (int i = 0; i < position - 1; i++) {
                n = n.next;
            }
            n.next = n.next.next;
            return (T) head;
        }
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }

}

