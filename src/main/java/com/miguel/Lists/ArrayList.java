package com.miguel.Lists;

// import java.util.Arrays;

import java.util.Arrays;

public class ArrayList<T>{
    private int size = 0;                                   //Iniciar o size com 0 elementos
    private static final int INICIAL_CAPACITY = 10;        // definir o maximo de elementos
    private Object elements[];                             //Nosso array

    public ArrayList(){                                    //Construtor do array que ira inicializar com o maximo de 10 elementos
        elements = new Object[INICIAL_CAPACITY];
    }


    /*O método indexOf () de ArrayList retorna o índice da primeira ocorrência do elemento especificado nesta lista,
    *   ou -1 se esta lista não contiver o elemento. */
    public int indexOf(Object obj) {
        if (obj == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null){
                    return i;
                }
            }
        }else{
            for(int i = 0; i < size; i++){
                if(obj.equals(elements[i])){
                    return i;
                }
            }
        }
        return -1;
    }
    /*Retorna true caso contenha o elemento or false caso nao tenha*/
    public boolean contains (Object element){
        return indexOf(element) != - 1;
    }
    /*elemento especifico deve ser inserido*/
    public boolean add(T element){
        int i = 0;
            if(size == 0){
                elements[0] = element;
                size++;
                return true;
            }
            if(elements.length == size){
                elements = Arrays.copyOf(elements,size*2);
            }
            while(elements[i] != null){
                i++;
            }
            elements[i] = element;
            size++;
            return true;
    }
    /*Insere um elemento esecifico na posição especifica nessa lista, desloca o elemento atualmente nessa posição(se houver)
     * e quaisquer elementos subsequente, à diretaadiciona um aos seus indices */
    public void addIndex(int index, T element) {
        if(index > size || index < 0){
            throw new IndexOutOfBoundsException("Index: "+ index + "," + "Size: "+ size);
        }
        if(elements.length == size){
            elements = Arrays.copyOf(elements,size*2);
        }
        for(int i = size; i > index; i--){
            elements[i] = elements[i - 1];
        }
        elements[index] = element;
        size ++;
    }

    /* O get vai ser usado para buscar um elemento. Precisamos especificar o índice ao chamar o método get e ele
     *  retona o valor presente no índice especifico*/

    public T get(int i){
        if(i >= size || i < 0){
            throw new IndexOutOfBoundsException("Index: "+ i + ", Size "+ i);
        }
        return (T) elements[i];
    }
    /* Size retorna o número de elementps nesta lista, isto é o tamanho da arrayList*/
    public int size(){
        return size;
    }

    /* O isEmpty vai verifciar se a arrayList esta vazia ou nao se estiver retorna true senao rretorna false */
    public boolean isEmpty(){
        if(size() == 0){
            return true;
        }
        return false;
    }

    /* O clear limpa arraylist */
    public void clear(){
        size = 0;
    }

    /* Remove o elemento na posição especifica nesta lista. Desloca quaisquer elementos subsequentes para a esquerda
    * (subtrai um dos seus índices)*/
    public T remove(int index){
        T temp = (T) elements[index];
        for(int i = index; i < size() - 1; i++){
            elements[i] = elements[i + 1];
        }
        size--;
        return temp;
    }

    /*Vai imprimar a arrauList*/
    public void printArrayList(){
        int i = 0;
        while(i != size){
            System.out.println(elements[i]);
            i++;
        }
    }

}



