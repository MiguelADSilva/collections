package com.miguel.Lists;

import java.util.Objects;

public class Node<T> {
     T value;
    Node<T> next;

    public Node(T value) {
        this.value = value;
    }

    public Node() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> node = (Node<?>) o;
        return Objects.equals(value, node.value) &&
                Objects.equals(next, node.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, next);
    }
}
